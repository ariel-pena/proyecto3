#include <stdio.h>
#include "universidad.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>

void consultar_ponderacion(facultad carreras[]){
	int opcion,i=0,codigocar;
	char nombreCar[80];

		//creamos un menu para que decida por cual metodobuscar la carrera deseada
		printf("-----------------------------------\n");
		printf("- seleccione su forma de busqueda -\n");
		printf("-----------------------------------\n");
		printf("- 1)buscar por nombre de carrera  -\n");
		printf("- 2)buscar por codigo de carrera  -\n");
		printf("-----------------------------------\n");
		scanf("%d",&opcion);
	
		//hacemos un switch para ejecutar la funcion respectiva segun la decicion del usiario 
		switch ( opcion ){
				case 1: printf("ingrense nombre de la carrera(cambie los espacios por (_) y (-)+ciudad de ser necesario )\n");
						scanf("%s",nombreCar);
						for (i=0;i<73;i++){
							if(strcmp(nombreCar,carreras[i].tcarreras.nombrecar)==0){
								printf("%s %s %d %d %d %d %d %d %d %d %d %f %d %d \n",carreras[i].nombrefacu,carreras[i].tcarreras.nombrecar,carreras[i].tcarreras.codigo,carreras[i].tcarreras.nem,carreras[i].tcarreras.rank,carreras[i].tcarreras.leng,carreras[i].tcarreras.mat,carreras[i].tcarreras.hist,carreras[i].tcarreras.cs,carreras[i].tcarreras.pond,carreras[i].tcarreras.psu,carreras[i].tcarreras.min,carreras[i].tcarreras.cupopsu,carreras[i].tcarreras.cupobea);
							}
						}
						break;

				case 2:  printf("ingrense codigo de la carrera\n");
						scanf("%d",&codigocar);
						printf("%d\n",codigocar);
						printf("%d\n",carreras[i].tcarreras.codigo);
						for (i=0;i<73;i++){
							if(codigocar==carreras[i].tcarreras.codigo){
								printf("%s %s %d %d %d %d %d %d %d %d %d %f %d %d \n",carreras[i].nombrefacu,carreras[i].tcarreras.nombrecar,carreras[i].tcarreras.codigo,carreras[i].tcarreras.nem,carreras[i].tcarreras.rank,carreras[i].tcarreras.leng,carreras[i].tcarreras.mat,carreras[i].tcarreras.hist,carreras[i].tcarreras.cs,carreras[i].tcarreras.pond,carreras[i].tcarreras.psu,carreras[i].tcarreras.min,carreras[i].tcarreras.cupopsu,carreras[i].tcarreras.cupobea);	
							}
						}
						break;						
	    }
}


void simular(facultad carreras[]){
	float pnem,prank,plen,pmat,phist,pcs,ponderacion,diferencia;
	int i=0;
	char nombreCar[80];

	//le pedimos al usuario que indique la carrera deseada para que vea los porcentajes de cada prueba
	printf("ingrense nombre de la carrera(cambie los espacios por (_) y (-)+ciudad de ser necesario )\n");
	scanf("%s",nombreCar);
	//recorremos mediante un for la estructura en busqueda de la carrera deseada
	for (i=0;i<73;i++){
		if(strcmp(nombreCar,carreras[i].tcarreras.nombrecar)==0){ 
			printf("%s %s %d %d %d %d %d %d %d %d %d %f %d %d \n",carreras[i].nombrefacu,carreras[i].tcarreras.nombrecar,carreras[i].tcarreras.codigo,carreras[i].tcarreras.nem,carreras[i].tcarreras.rank,carreras[i].tcarreras.leng,carreras[i].tcarreras.mat,carreras[i].tcarreras.hist,carreras[i].tcarreras.cs,carreras[i].tcarreras.pond,carreras[i].tcarreras.psu,carreras[i].tcarreras.min,carreras[i].tcarreras.cupopsu,carreras[i].tcarreras.cupobea);
				
				//pedimos al usuario que ingrese sus puntajes
				printf("ingrese su nem\n");
				scanf("%f",&pnem);
				printf("ingrese su rankin\n");
				scanf("%f",&prank);
				printf("ingrese su puntaje en lenguaje\n");
				scanf("%f",&plen);
				printf("ingrese su puntaje en matematicas\n");
				scanf("%f",&pmat);
				printf("ingrese su puntaje en historia\n");
				scanf("%f",&phist);
				printf("ingrese su puntaje en ciencias\n");
				scanf("%f",&pcs);	

				//hacemos el calcullo de la ponderacion del estudiante
				ponderacion=((pnem*carreras[i].tcarreras.nem)/100)+((prank*carreras[i].tcarreras.rank)/100)+((plen*carreras[i].tcarreras.leng)/100)+((pmat*carreras[i].tcarreras.mat)/100)+((phist*carreras[i].tcarreras.hist)/100)+((pcs*carreras[i].tcarreras.cs)/100);
				printf("su ponderacion es: %f\n",ponderacion); //mostramos por pantalla la ponderacion del usuario

				//hacemos el calculo de la diferencia entre la ponderacion del usuario con el puntaje minimo de ingreso del año anterior
				diferencia=ponderacion-carreras[i].tcarreras.min;
				printf("%f\n",diferencia);	//mostramos por pantalla la diferencia

		}
	}
}



void ponderaciones_facultad(facultad carreras[]){
		char nombreFac[49];
		int i=0;

		//le pedimos al usuario que ingrese el nombre de la facultad deseada
		printf("ingrense nombre de la facultad(cambie los espacios por (_) )\n");
		scanf("%s",nombreFac);

		//recorremos la estructura para poder buscar las carreras asociadas a la facultad deseada
		for (i=0;i<73;i++){
			if(strcmp(nombreFac,carreras[i].nombrefacu)==0){
				printf("%s %s %d %d %d %d %d %d %d %d %d %f %f %d %d \n",carreras[i].nombrefacu,carreras[i].tcarreras.nombrecar,carreras[i].tcarreras.codigo,carreras[i].tcarreras.nem,carreras[i].tcarreras.rank,carreras[i].tcarreras.leng,carreras[i].tcarreras.mat,carreras[i].tcarreras.hist,carreras[i].tcarreras.cs,carreras[i].tcarreras.pond,carreras[i].tcarreras.psu,carreras[i].tcarreras.max,carreras[i].tcarreras.min,carreras[i].tcarreras.cupopsu,carreras[i].tcarreras.cupobea);
			}
		}
}		
		
void menu(facultad carreras[]){
	int opcion,i=0;
	FILE *file;
	
	//importamos los datos del archivo txt
	if((file=fopen("admision.txt","r"))==NULL){
		printf("\nerror al arbir el archivo");
		exit(0);
	}
	else{
		for (i=0;i<73;i++) {
			fscanf(file," %s %s %d %d %d %d %d %d %d %d %d %f %f %d %d ",carreras[i].nombrefacu,carreras[i].tcarreras.nombrecar,&carreras[i].tcarreras.codigo,&carreras[i].tcarreras.nem,&carreras[i].tcarreras.rank,&carreras[i].tcarreras.leng,&carreras[i].tcarreras.mat,&carreras[i].tcarreras.hist,&carreras[i].tcarreras.cs,&carreras[i].tcarreras.pond,&carreras[i].tcarreras.psu,&carreras[i].tcarreras.max,&carreras[i].tcarreras.min,&carreras[i].tcarreras.cupopsu,&carreras[i].tcarreras.cupobea);		
		}
		fclose(file);
	}
	do{
		printf("-----------------------------------------\n");
		printf("-                  menu                 -\n");
		printf("-----------------------------------------\n");
		printf("- 1)Consultar ponderacion de la carrera -\n");
		printf("- 2)Simular postulacion a la carrera    -\n");
		printf("- 3)Mostrar ponderaciones a la facultad -\n");
		printf("- 4)Salir                               -\n");
		printf("-----------------------------------------\n");
		printf("-         selecione una opcion          -\n");
		printf("-----------------------------------------\n");
		scanf("%d",&opcion);

		switch ( opcion ){
				case 1: consultar_ponderacion(carreras); //muestra la ponderacion de la carrera segun su nombre o codigo.
						break;

				case 2:  simular(carreras);//ingresa datos de la simulacion.
						break;

				case 3: ponderaciones_facultad(carreras); //muestra todas las ponderaciones de las carreras de la facultad.
						break;
	    }
	}
	while(opcion!=4);
}	
	
	
int main(){
	facultad carreras[73];
	menu(carreras);
	return EXIT_SUCCESS;
}