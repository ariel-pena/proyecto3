/*** universidad.h***/

#ifndef UNIVERSIDAD_H
#define UNIVERSIDAD_H

//definimos los elementos de la estructura carrera
typedef struct {
	char nombrecar[80];
	int codigo;
	int nem,rank,leng,mat,hist,cs,pond,psu,cupopsu,cupobea;
	float max,min;
} carrera; 

//creamos una estructura de tipo facultad donde definimos el contenido de esta
typedef struct{
	char nombrefacu[49];
	carrera tcarreras; 
} facultad;
#endif //universidad_H

